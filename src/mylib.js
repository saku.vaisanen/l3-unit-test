module.exports = {
    sum: (a, b) => {
        return a + b;
    },

    // only does positive exponents
    power: (a, b) => {
        let total = a;
        for(i = 0; i < b-1; i++){
            total *= a;
        }
        return total;
    }
}