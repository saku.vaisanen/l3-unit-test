const expect = require('chai').expect;
const assert = require('chai').assert;
const should = require('chai').should();
const mylib = require('../src/mylib');

describe('Unit testing mylib.js', () => {

    let myvar = undefined;

    before(() => {
        console.log('before');
        myvar = 1;
    })

    it('Expect: Should return 2 when using sum function with a=1, b=1', () => {
        const result = mylib.sum(1,1);
        expect(result).to.equal(2);
    })

    it('Expect: Should return 1024 when using power function with a=2, b=10', () => {
        const result = mylib.power(2, 10);
        expect(result).to.equal(1024)
    })

    it('Assert: Should return 2 when using sum function with a=1, b=1', () => {
        assert(mylib.sum(1,1) === 2);
    })

    it('Assert: Should return 1024 when using power function with a=2, b=10', () => {
        assert(mylib.power(2, 10) === 1024);
    })

    it.skip('Myvar should exist', () => {
        should.exist(myvar);
    })

    it.skip('Assert foo is not bar', () => {
        assert('foo' !== 'bar');
    })

    after(() => {
        console.log('after');
    })
})